package com.jasperassistant.designer.viewer.actions;

import java.io.File;

import org.eclipse.jface.resource.ImageDescriptor;

import com.jasperassistant.designer.viewer.IReportViewer;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.export.JRExportProgressMonitor;
import net.sf.jasperreports.engine.export.oasis.JROdtExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOdtReportConfiguration;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

public class ExportAsOdtAction extends AbstractExportAction {

	private static final ImageDescriptor ICON = ImageDescriptor.createFromFile(ExportAsPdfAction.class,
			"images/save.gif"); //$NON-NLS-1$

	private static final ImageDescriptor DISABLED_ICON = ImageDescriptor.createFromFile(ExportAsPdfAction.class,
			"images/saved.gif"); //$NON-NLS-1$

	public ExportAsOdtAction(IReportViewer viewer) {
		super(viewer);

		setText(Messages.getString("ExportAsOdtAction.label")); //$NON-NLS-1$
		setToolTipText(Messages.getString("ExportAsOdtAction.tooltip")); //$NON-NLS-1$
		setImageDescriptor(ICON);
		setDisabledImageDescriptor(DISABLED_ICON);

		setFileExtensions(new String[] { "*.odt" });
		setFilterNames(new String[] { Messages.getString("ExportAsOdtAction.filterName") });
		setDefaultFileExtension("odt");
	}

	public void exportWithProgress(File file, JRExportProgressMonitor monitor) throws JRException {
		JROdtExporter exporter = new JROdtExporter();
		SimpleOdtReportConfiguration config = new SimpleOdtReportConfiguration();
		config.setProgressMonitor(monitor);

		exporter.setExporterInput(new SimpleExporterInput(getReportViewer().getDocument()));
		exporter.setConfiguration(config);
		exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(file));
		exporter.exportReport();
	}
}
