package com.jasperassistant.designer.viewer.actions;

import java.io.File;

import org.eclipse.jface.resource.ImageDescriptor;

import com.jasperassistant.designer.viewer.IReportViewer;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.export.JRExportProgressMonitor;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.export.SimpleDocxReportConfiguration;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

public class ExportAsDocxAction extends AbstractExportAction {

	private static final ImageDescriptor ICON = ImageDescriptor.createFromFile(ExportAsPdfAction.class,
			"images/save.gif"); //$NON-NLS-1$

	private static final ImageDescriptor DISABLED_ICON = ImageDescriptor.createFromFile(ExportAsPdfAction.class,
			"images/saved.gif"); //$NON-NLS-1$

	public ExportAsDocxAction(IReportViewer viewer) {
		super(viewer);

		setText(Messages.getString("ExportAsDocxAction.label")); //$NON-NLS-1$
		setToolTipText(Messages.getString("ExportAsDocxAction.tooltip")); //$NON-NLS-1$
		setImageDescriptor(ICON);
		setDisabledImageDescriptor(DISABLED_ICON);

		setFileExtensions(new String[] { "*.docx" });
		setFilterNames(new String[] { Messages.getString("ExportAsDocxAction.filterName") });
		setDefaultFileExtension("docx");
	}

	public void exportWithProgress(File file, JRExportProgressMonitor monitor) throws JRException {
		JRDocxExporter exporter = new JRDocxExporter();
		SimpleDocxReportConfiguration config = new SimpleDocxReportConfiguration();
		config.setProgressMonitor(monitor);

		exporter.setExporterInput(new SimpleExporterInput(getReportViewer().getDocument()));
		exporter.setConfiguration(config);
		exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(file));
		exporter.exportReport();
	}
}
