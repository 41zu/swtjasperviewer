package com.jasperassistant.designer.viewer.actions;

import java.text.MessageFormat;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;

import com.jasperassistant.designer.viewer.IReportViewer;
import com.jasperassistant.designer.viewer.ViewerApp;

public class ShowAboutAction extends AbstractReportViewerAction {

	String URL = "https://gitlab.com/41zu/swtjasperviewer/";
	String LICENSE = "LGPL";

	public ShowAboutAction(IReportViewer viewer) {
		super(viewer);
		setText(Messages.getString("ShowInfoAction.label"));
	}

	/**
	 * create a dialog with ok button and a question icon
	 */
	@Override
	protected void runBusy() {
		MessageBox dialog = new MessageBox(Display.getCurrent().getActiveShell(), SWT.ICON_INFORMATION | SWT.OK);
		dialog.setText("About swtjasperviewer");
		dialog.setMessage(MessageFormat.format(Messages.getString("ShowInfoAction.message"),
				new Object[] { ViewerApp.getVersion(), URL, LICENSE }));
		dialog.open();
	}

	/**
	 * always return 'true' because we always want this entry to be enabled
	 */
	@Override
	protected boolean calculateEnabled() {
		return true;
	}
}
