JasperReports 5.6.1
===================
commons-beanutils-1.8.0.jar
commons-collections-3.2.2.jar
commons-digester-2.1.jar
commons-logging-1.1.1.jar
itext-2.1.7.js2.jar
jasperreports-5.6.1.jar
jcommon-1.0.24.jar
jdtcore-3.1.0.jar
jfreechart-1.0.19.jar
poi-3.7.jar
poi-ooxml-3.7.jar
xml-apis-1.3.02.jar

SWT 3.0 Win32
=============
win32/*

SWT 3.0 Linux GTK
=================
linux-gtk/*

JFace 3.0
=========
jface.jar
osgi.jar
runtime.jar
