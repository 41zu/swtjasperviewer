REM
REM Demo launcher
REM

java -Djava.awt.headless=true -Djava.library.path=lib\win32 -classpath swtjasperviewer-1.5.1.jar;lib\commons-beanutils-1.8.0.jar;lib\commons-collections-3.2.2.jar;lib\commons-digester-2.1.jar;lib\commons-logging-1.1.1.jar;lib\itext-2.1.7.js2.jar;lib\jasperreports-5.6.1.jar;lib\jcommon-1.0.24.jar;lib\jdtcore-3.1.0.jar;lib\jfreechart-1.0.19.jar;lib\poi-3.7.jar;lib\poi-ooxml-3.7.jar;lib\xml-apis-1.3.02.jar;lib\win32\swt.jar;lib\eclipse\jface.jar;lib\eclipse\osgi.jar;lib\eclipse\runtime.jar com.jasperassistant.designer.viewer.ViewerApp -Fdemo\FirstJasper.jrprint
